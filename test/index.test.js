//
// index.test.js
//
// testing the top-level. very briefly

const assert = require('assert');
const proxyquire = require('proxyquire');
const sinon = require('sinon');
const _ = require('lodash');

const subscribe = sinon.spy(async () => 0);
const getProviderDetails = sinon.spy(async () => 0);
const redeemCode = sinon.spy(async () => 0);
const yggioConnect = proxyquire('../src/index', {
  './logger': {warn: () => 0},
  './service-provider': {
    subscribe,
    getProviderDetails,
    redeemCode,
  },
  './config-validator': {
    assertValidUrl: () => 0, assertValidRefreshCallback: () => 0,
    assertValidAccountConfig: () => 0, assertValidProviderConfig: () => 0,
  },
});

describe('testing the MAIN export (top-level index.js)', () => {

  it('exports.provider.redeemCode is mapped straight from serviceProvider', () => {
    assert(yggioConnect.provider.redeemCode === redeemCode, 'architecture has changed');
  });

  it('exports.provider.subscribe is mapped straight from serviceProvider', () => {
    assert(yggioConnect.provider.subscribe === subscribe, 'architecture has changed');
  });

  it('exports.provider.getDetails is mapped straight from serviceProvider', () => {
    assert(yggioConnect.provider.getDetails === getProviderDetails, 'architecture has changed');
  });

  it('exports.rawRoutes is just the exports of "./routes"', () => {
    assert(yggioConnect.rawRoutes === require('../src/routes'), 'architecture has changed');
  });

  it('exports.routes and exports.rawRoutes share common keys', () => {
    const wrappedkeys = _.keys(yggioConnect.routes);
    const rawKeys = _.keys(yggioConnect.rawRoutes);
    assert(_.isEqual(wrappedkeys, rawKeys));
  });

  it('prior to init, all routes should throw uninitialized error', () => {
    const message = 'Evaluate wrapped route: service provider is not initialized';
    return Promise.all(_.map(yggioConnect.routes, route => {
      return route()
        .then(() => Promise.reject(new Error('Should not have succeeded')))
        .catch(err => {
          assert(err.message === message)
        });
    }));
  });

});
