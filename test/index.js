// test/index.js

describe('yggio-connect tests', () => {
  require('./config-validator.test');
  require('./yggio-request.test');
  require('./routes.test');
  require('./service-provider.test');
  // and the top level
  require('./index.test');
});
