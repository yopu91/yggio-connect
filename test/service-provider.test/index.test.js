// wrap-refresh.test.js

const _ = require('lodash');
const proxyquire = require('proxyquire');
const sinon = require('sinon');
const assert = require('assert');

// create proxies for 'require' stuff
const login = sinon.spy(async () => ({login: true}));
const registerProvider = sinon.spy(async () => ({register: true}));
const refreshAccessToken = sinon.spy(async (user, refreshToken) => {
  if (refreshToken === 'invalid') {
    throw new Error('refreshAccessToken failed');
  }
  return {refresh: true};
});
const subscribe = sinon.spy(async () => ({subscribe: true}));

const serviceProvider = proxyquire('../../src/service-provider', {
  './recursive-login': login,
  './register-provider': registerProvider,
  '../logger': {error: () => 0},
  '../routes': {refreshAccessToken, subscribe},
});

describe('service-provider/index (the top level)', () => {
  const initErrorMessage = 'Provider has not been initialized';
  describe('pre-initialization', () => {
    it('isInitialized() returns false', () => {
      assert(!serviceProvider.isInitialized());
    });
    it('getProviderDetails() throws error', () => {
      assert.throws(() => getProviderDetails());
    });
    it('subscribe throws initialization error', () => {
      return serviceProvider.subscribe()
        .then(res => Promise.reject('should not have succeeded'))
        .catch(err => {
          assert(err.message === initErrorMessage);
        });
    });
    it('wrapRoute throws initialization error', () => {
      assert.throws(() => serviceProvider.wrapRoute());
    });
  });

  describe('post-initialization', () => {
    const accountConfig = {};
    const providerConfig = {};
    const refreshCallback = sinon.spy(() => 0);
    let entities;
    before(async () => {
      entities = await serviceProvider.init(
        accountConfig,
        providerConfig,
        refreshCallback,
      );
    });

    it('isInitialized() returns true', () => {
      assert(serviceProvider.isInitialized());
    });

    it('getProviderDetails() return the entities', () => {
      const expectedEntities = {
        'account': {login: true},
        'provider': {register: true},
      };
      const entities = serviceProvider.getProviderDetails();
      assert(_.isEqual(entities, expectedEntities));
    });

    it('login entities should be well-defined', () => {
      assert(_.isEqual(entities.account, {login: true}));
      assert(_.isEqual(entities.provider, {register: true}));
      assert(login.calledOnce);
      assert(registerProvider.calledOnce);
      assert(refreshAccessToken.notCalled);
    });

    it('subscribe should succeed trivially', () => {
      return serviceProvider.subscribe()
        .then(res => {
          assert(_.isEqual(res, {subscribe: true}));
          assert(subscribe.calledOnce);
          assert(refreshAccessToken.notCalled);
          subscribe.resetHistory();
        });
    });

    describe('wrap route with refresh --- this one will be tested thoroughly', () => {
      // set up the relevant fake routes
      const validUser = {accessToken: 'xxx', refreshToken: 'xxx'};
      const workingRoute = sinon.spy(async () => ({workingRoute: true}));
      const brokenRoute = sinon.spy(async () => Promise.reject(new Error('General error')));
      const refreshableRoute = sinon.spy(async () => Promise.reject({statusCode: 401}));
      let wrappedWorkingRoute = null;
      let wrappedBrokenRoute = null;
      let wrappedRefreshableRoute = null;
      before(() => {
        wrappedWorkingRoute = serviceProvider.wrapRoute(workingRoute);
        wrappedBrokenRoute = serviceProvider.wrapRoute(brokenRoute);
        wrappedRefreshableRoute = serviceProvider.wrapRoute(refreshableRoute);
      });

      it('workingRoute should succeed (trivially)', () => {
        return wrappedWorkingRoute()
          .then(res => {
            assert(_.isEqual(res, {workingRoute: true}));
            assert(refreshAccessToken.notCalled);
            assert(workingRoute.calledOnce);
            workingRoute.resetHistory();
          });
      });

      it('brokenRoute should fail predictably without valid user', () => {
        return wrappedBrokenRoute()
          .then(res => Promise.reject(new Error('should have failed')))
          .catch(err => {
            assert(err.message === 'General error');
            assert(refreshAccessToken.notCalled);
            assert(brokenRoute.calledOnce);
            brokenRoute.resetHistory();
          });
      });

      it('brokenRoute should fail predictably WITH valid user', () => {
        return wrappedBrokenRoute(validUser)
          .then(res => Promise.reject(new Error('should have failed')))
          .catch(err => {
            assert(err.message === 'General error');
            assert(refreshAccessToken.notCalled);
            assert(brokenRoute.calledOnce);
            brokenRoute.resetHistory();
          });
      });

      it('refreshableRoute should fail trivially WITHOUT valid user', () => {
        return wrappedRefreshableRoute()
          .then(res => Promise.reject(new Error('should have failed')))
          .catch(err => {
            assert(err.statusCode === 401);
            assert(refreshAccessToken.notCalled);
            assert(refreshableRoute.calledOnce);
            refreshableRoute.resetHistory();
          });
      });

      it('refreshableRoute should fail special-like if user.refreshToken is missing', () => {
        return wrappedRefreshableRoute(_.omit(validUser, 'refreshToken'))
        .then(res => Promise.reject(new Error('should have failed')))
        .catch(err => {
          assert(err.message === 'refresh token is missing');
          assert(refreshAccessToken.notCalled);
          assert(refreshableRoute.calledOnce);
          refreshableRoute.resetHistory();
        });
      });

      it('refreshableRoute should return refreshError if refresh fails', () => {
        return wrappedRefreshableRoute(_.assign({}, validUser, {refreshToken: 'invalid'}))
          .then(res => Promise.reject(new Error('should have failed')))
          .catch(err => {
            assert(err.message === 'refreshAccessToken failed');
            assert(refreshAccessToken.calledOnce);
            refreshAccessToken.resetHistory();
            assert(refreshableRoute.calledOnce);
            refreshableRoute.resetHistory();
          });
      });

      it('refreshableRoute should return {statusCode: 401} upon refresh success, but refreshCallback should have been called', () => {
        return wrappedRefreshableRoute(validUser)
        .then(res => Promise.reject(new Error('should have failed')))
        .catch(err => {
          assert(err.statusCode === 401);
          assert(refreshAccessToken.calledOnce);
          refreshAccessToken.resetHistory();
          assert(refreshableRoute.calledTwice); // TWICE!!
          refreshableRoute.resetHistory();
        });
      });
    });
  });

});
