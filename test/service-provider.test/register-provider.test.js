// register-provider.test.js

const proxyquire = require('proxyquire');
const sinon = require('sinon');
const assert = require('assert');
const _ = require('lodash');

const registerProvider = sinon.spy(async () => ({register: true}));
const setLogo = sinon.spy(async() => {});

const register = proxyquire('../../src/service-provider/register-provider', {
  // we don't want to test the actual routes
  '../routes': {registerProvider, setLogo},
  // skip the config validation
  '../config-validator': {assertValidProviderConfig: () => 0},
  // and shut up the logger
  '../logger': {log: () => 0, info: () => 0, warn: () => 0, error: () => 0},
});

describe('register-provider.test.js', () => {

  it('should trivially succeed', () => {
    const providerDetails = {
      redirectUris: {},
    };
    return register({serviceAccount: true}, providerDetails)
      .then(res => {
        const expectedRes = {
          register: true,
          redirectUris: {},
        };
        assert(_.isEqual(res, expectedRes));
        assert(registerProvider.calledOnce);
        assert(setLogo.notCalled);
        registerProvider.resetHistory();
      });
  });

  it('should trivially succeed also with specified logoPath', () => {
    const providerDetails = {
      redirectUris: {},
      logoPath: 'xxx',
    };
    return register({serviceAccount: true}, providerDetails)
      .then(res => {
        const expectedRes = {
          register: true,
          redirectUris: {},
        };
        assert(_.isEqual(res, expectedRes));
        assert(registerProvider.calledOnce);
        assert(setLogo.calledOnce);
        registerProvider.resetHistory();
        setLogo.resetHistory();
      });
  });
});
