// service-provider.test/index.js

describe('testing the service provider modules', () => {
  require('./recursive-login.test');
  require('./register-provider.test');
  // and LASTLY test the index with the wrap and setup stuff
  require('./index.test');
});
