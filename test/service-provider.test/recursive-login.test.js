// recursive-login.test.js

const assert = require('assert');
const _ = require('lodash');
const proxyquire = require('proxyquire');
const sinon = require('sinon');

const login = sinon.spy(async ({numLogin}) => {
  const numCalls = login.callCount;
  if (numCalls >= numLogin) {
    return {loggedIn: numCalls};
  } else {
    throw new Error('login failed: ' + numCalls);
  }
});

const register = sinon.spy(async ({registrationWorks}) => {
  if (!registrationWorks) {
    throw new Error('registration fails');
  } else {
    return {register: true};
  }
});

const recursiveLogin = proxyquire('../../src/service-provider/recursive-login', {
  // we don't want to test the actual routes
  '../routes': {login, register},
  // skip the config validation
  '../config-validator': {assertValidAccountConfig: () => 0},
  // and shut up the logger
  '../logger': {log: () => 0, info: () => 0, warn: () => 0, error: () => 0},
});

describe('recursive-login', () => {

  it('should finish after registration upon reg success', () => {
    const expectedRes = {registrationWorks: true, register: true};
    return recursiveLogin({registrationWorks: true})
      .then(res => {
        assert(_.isEqual(res, expectedRes));
        assert(register.calledOnce, 'register should be called once');
        assert(login.notCalled, 'login should not have been called');
        register.resetHistory();
      });
  });

  it('should finish after first login (which succeeds)', () => {
    const expectedRes = {numLogin: 1, loggedIn: 1};
    return recursiveLogin({numLogin: 1})
      .then(res => {
        assert(_.isEqual(res, expectedRes));
        assert(register.calledOnce, 'register should be called once');
        assert(login.calledOnce, 'login should be called once');
        register.resetHistory();
        login.resetHistory();
      });
  });

  it('should finish after 5 logins (which succeeds)', () => {
    const expectedRes = {numLogin: 5, loggedIn: 5, retryDelay: 0};
    return recursiveLogin({numLogin: 5, retryDelay: 0})
      .then(res => {
        assert(_.isEqual(res, expectedRes));
        assert(register.callCount === 5);
        assert(login.callCount === 5);
        register.resetHistory();
        login.resetHistory();
      });
  });
});
