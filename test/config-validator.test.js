// config-validator.test.js

const assert = require('assert');
const _ = require('lodash');

const configValidator = require('../src/config-validator');

describe('config-validator', () => {

  describe('assertValidUrl', () => {
    it('should fail without valid config.url', () => {
      assert.throws(() => configValidator.assertValidUrl());
      assert.throws(() => configValidator.assertValidUrl({}));
      assert.throws(() => configValidator.assertValidUrl({url: 2}));
    });
    it('should pass with a valid config.url', () => {
      configValidator.assertValidUrl({url: 'xxx'});
    });
  });

  describe('assertValidAccountConfig', () => {
    it('should fail without config', () => {
      assert.throws(() => configValidator.assertValidAccountConfig());
    });
    it('should fail without username', () => {
      assert.throws(() => configValidator.assertValidAccountConfig({
        password: 'xxx'
      }));
    });
    it('should fail without password', () => {
      assert.throws(() => configValidator.assertValidAccountConfig({
        username: 'xxx'
      }));
    });
    it('should pass with a valid config', () => {
      configValidator.assertValidAccountConfig({
        username: 'xxx',
        password: 'xxx',
      });
    });
  });

  describe('assertValidProviderConfig', () => {
    const minimal = {
      name: 'xxx',
      redirectUris: ['xxx'],
    };
    const maximal = {
      name: 'xxx',
      info: 'xxx',
      channel: {
        url: 'xxx',
        route: 'xxx',
      },
      redirectUris: {
        'xxx': 'xxx',
        'browser': 'xxx',
        'app': 'xxx',
      },
    };
    it('should fail without config', () => {
      assert.throws(() => configValidator.assertValidProviderConfig());
    });
    it('should pass with a valid minimal config', () => {
      configValidator.assertValidProviderConfig(minimal);
    });
    it('should pass with a valid maximal config', () => {
      configValidator.assertValidProviderConfig(maximal);
    });
    it('should fail if name is missing', () => {
      const missing = _.omit(minimal, 'name');
      assert.throws(() => configValidator.assertValidProviderConfig(missing));
    });
    it('should fail if redirectUris is missing', () => {
      const missing = _.omit(minimal, 'redirectUris');
      assert.throws(() => configValidator.assertValidProviderConfig(missing));
    });
    it('should fail if redirectUris is empty', () => {
      const empty = _.assign({}, minimal, {redirectUris: []});
      assert.throws(() => configValidator.assertValidProviderConfig(empty));
    });
    it('should fail if channel.url is missing (if channel included)', () => {
      const missing = _.assign({}, maximal, {
        channel: _.omit(maximal.channel, 'url'),
      });
      assert.throws(() => configValidator.assertValidProviderConfig(missing));
    });
    it('should pass if channel.route is missing (if channel included)', () => {
      const missing = _.assign({}, maximal, {
        channel: _.omit(maximal.channel, 'route'),
      });
      configValidator.assertValidProviderConfig(missing);
    });
  });

  describe('assertValidRefreshCallback', () => {
    it('should fail without valid refreshCallback', () => {
      assert.throws(() => configValidator.assertValidRefreshCallback());
      assert.throws(() => configValidator.assertValidRefreshCallback({}));
    });
    it('should pass with a valid refreshCallback', () => {
      configValidator.assertValidRefreshCallback(() => {});
    });
  });
});
