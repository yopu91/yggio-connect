// yggio-request.test.js

const assert = require('assert');
const proxyquire = require('proxyquire');
const _ = require('lodash');

const yggioRequest = proxyquire('../src/yggio-request', {
  'request-promise': async () => {'done'},
});

const methods = ['get', 'post', 'put', 'delete'];

describe('testing yggio-request.js', () => {

  it('should include function: setYggioUrl', () => {
    assert(yggioRequest.setYggioUrl, 'setYggioUrl missing');
  });

  it('should include REST methods: GET, POST, PUT, DELETE', () => {
    _.each(methods, method => {
      assert(yggioRequest[method], method.toUpperCase() + ' missing');
    });
  });

  it('REST methods should all throw error when yggioUrl is not set', () => {
    return Promise.all(_.map(methods, method => {
      return yggioRequest[method]()
        .then(() => new Error('should not work'))
        .catch(err => {
          assert(err.message === 'yggioUrl is not set', 'did not get yggioUrl error ')
        });
    }));
  });

  it('REST methods should throw error if uri is missing', () => {
    yggioRequest.setYggioUrl('test-url');
    return Promise.all(_.map(methods, method => {
      return yggioRequest[method]()
        .then(() => new Error('should not work'))
        .catch(err => {
          assert(err.message === 'yggio api uri is missing', 'did not get yggioUrl error ');
        });
    }));
  });

});
