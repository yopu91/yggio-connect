// routes.test.js

const _ = require('lodash');
const assert = require('assert');
const proxyquire = require('proxyquire');

const routes = proxyquire('../src/routes', {
  'fs': {
    createReadStream: () => 'fake_file_data',
  },
  './yggio-request': {
    get: async () => 'get',
    post: async () => 'post',
    put: async () => 'put',
    delete: async () => 'delete',
  }
});


const nonTokenRoutes = [
  'login',
  'register',
  'refreshAccessToken',
  'oauthCode',
];

const tokenRoutes = [
  // provider
  'setLogo',
  'registerProvider',
  'subscribe',
  // iotnodes
  'command',
  'getMe',
  'logoutFromYggio',
  'getNode',
  'getNodes',
  'getAccessibleNodes',
  'createNode',
  'updateNode',
  'deleteNode',
  // oauth
  'checkOAuthStatus',
  // states
  'getRulesStates',
  'setRulesState',
  // rules
  'getRule',
  'getRules',
  'executeRule',
  'getScenarioTemplates',
  'createScenario',
  'getRulesConditions',
  'updateRulesCondition',
  'createRulesCondition',
  'getRulesActions',
  'createRulesAction',
  'updateRulesAction',
  'deleteRulesAction',
  'createRule',
  'deleteRule',
  'updateRule',
  'addRuleTrigger',
  'addRuleEvent',
  // Contacts
  'getContacts',
  'setContacts',
]
describe('testing routes', () => {

  describe('basic route auditing: do we have the routes we expect?', () => {
    it('should have expected number of routes', () => {
      const numRoutes = _.keys(routes).length;
      const expectedNumRoutes = tokenRoutes.length + nonTokenRoutes.length;
      assert(numRoutes === expectedNumRoutes, 'Got ' + numRoutes + ' routes, expected ' + expectedNumRoutes);
    });

    it('should have expected names of routes', () => {
      const routeKeys = _.keys(routes);
      const expectedKeys = tokenRoutes.concat(nonTokenRoutes);
      const diff1 = _.difference(routeKeys, expectedKeys);
      assert(!diff1.length, 'we have unexpected routes: ' + diff1.toString());
      const diff2 = _.difference(expectedKeys, routeKeys);
      assert(!diff2.length, 'we expected but did not find routes: ' + diff2.toString());
    });
  });

  describe('routes validate input parameters', () => {
    it('all tokenRoutes check for user.accessToken', () => {
      const expectedMessage = 'The user access token is missing (user.accessToken)';
      return Promise.all(_.map(tokenRoutes, routeKey => {
        return routes[routeKey]({})
          .then(() => Promise.reject(new Error('This should have failed')))
          .catch(err => {
            assert(err.message === expectedMessage, routeKey + ' - invalid message: ' + err.message);
          });
      }));
    });

    it('updateRulesAction checks for valid updates object', () => {
      const user = {accessToken: 'xxx'};
      const expectedMessage = 'invalid updates object';
      return routes.updateRulesAction(user)
        .then(() => Promise.reject(new Error('This should have failed')))
        .catch(err => {
          assert(err.message === expectedMessage, 'updateRulesAction - invalid message: ' + err.message);
        });
    });

    it('updateRule checks for valid updates object', () => {
      const user = {accessToken: 'xxx'};
      const expectedMessage = 'invalid updates object';
      return routes.updateRule(user)
        .then(() => Promise.reject(new Error('This should have failed')))
        .catch(err => {
          assert(err.message === expectedMessage, 'updateRule - invalid message: ' + err.message);
        });
    });

    it('login route require "username" and "password" fields', () => {
      const invalids = [{username: 'xxx'}, {password: 'xxx'}];
      const expectedMessage = 'username or password is missing';
      return Promise.all(_.map(invalids, inv => {
        return routes.login(inv)
          .then(() => Promise.reject(new Error('This should have failed')))
          .catch(err => {
            assert(err.message === expectedMessage, 'login - invalid message: ' + err.message);
          });
      }));
    });

    it('register route require "username" and "password" fields', () => {
      const invalids = [{username: 'xxx'}, {password: 'xxx'}];
      const expectedMessage = 'username or password is missing';
      return Promise.all(_.map(invalids, inv => {
        return routes.register(inv)
          .then(() => Promise.reject(new Error('This should have failed')))
          .catch(err => {
            assert(err.message === expectedMessage, 'register - invalid message: ' + err.message);
          });
      }));
    });

    it('refreshAccessToken route requires valid provider and refreshToken', () => {
      const invalids = [
        [{client_id: 'xxx', secret: 'xxx'}, null],
        [{client_id: 'xxx', secret: null}, 'refreshToken'],
        [{client_id: null, secret: 'xxx'}, 'refreshToken'],
      ];
      const expectedMessages = ['invalid provider object', 'refreshToken is missing'];
      return Promise.all(_.map(invalids, inv => {
        return routes.refreshAccessToken(...inv)
          .then(() => Promise.reject(new Error('This should have failed')))
          .catch(err => {
            assert(_.includes(expectedMessages, err.message), 'register - invalid message: ' + err.message);
          });
      }));
    });
  });

  describe('routes nominally work with correct inputs', () => {
    const specialTokenRoutes = [
      'setLogo',
      'updateRulesAction',
      'updateRule',
      'subscribe',
    ];
    it('all tokenRoutes (which have no special validation)', () => {
      const user = {accessToken: 'xxx'};
      const nonSpecials = _.difference(tokenRoutes, specialTokenRoutes);
      return Promise.all(_.map(nonSpecials, key => {
        return routes[key](user)
          .catch(err => {
            assert(false, key + ' failed: ' + err.message);
          });
      }));
    });

    it('setLogo needs user, providerId and logoPath', () => {
      const user = {accessToken: 'xxx'};
      const providerId = 'xxx';
      const logoPath = 'xxx';
      return routes.setLogo(user, providerId, logoPath);
    });

    it('updateRulesAction needs user and valid updates object', () => {
      const user = {accessToken: 'xxx'};
      const updates = {_id: 'xxx'};
      return routes.updateRulesAction(user, updates);
    });

    it('updateRule needs user and valid updates object', () => {
      const user = {accessToken: 'xxx'};
      const updates = {_id: 'xxx'};
      return routes.updateRule(user, updates);
    });

    it('subscribe needs a user and valid subscription', () => {
      const user = {accessToken: 'xxx'};
      const subscription = {
        provider: 'xxx',
        iotnode: 'xxx',
        type: 'xxx',
      };
      return routes.subscribe(user, subscription);
    });

    it('login needs valid credentials', () => {
      const credentials = {username: 'xxx', password: 'xxx'};
      return routes.login(credentials);
    });

    it('register needs valid credentials', () => {
      const credentials = {username: 'xxx', password: 'xxx'};
      return routes.register(credentials);
    });

    it('refreshAccessToken needs valid provider and refreshToken', () => {
      const provider = {client_id: 'xxx', secret: 'xxx'};
      const refreshToken = 'xxx';
      return routes.refreshAccessToken(provider, refreshToken);
    });
  });
});
