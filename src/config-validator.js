// config-validator.js

// The purpose of this module is really a means to give feedback to user/developer
// when invalid configs are used

const assert = require('assert');
const _ = require('lodash');

const assertValidUrl = config => {
  const url = config.url;
  assert(typeof url === 'string', 'yggio url is missing or not a string');
};

const assertValidAccountConfig = config => {
  assert(config, 'account config is missing');
  const username = config.username;
  assert(typeof username === 'string', 'username is missing or not a string');
  const password = config.password;
  assert(typeof password === 'string', 'password is missing or not a string');
};

const assertValidProviderConfig = config => {
  assert(config, 'provider details are missing');
  const name = config.name;
  assert(typeof name === 'string', 'name is missing or not a string');
  const info = config.info;
  if (info) {
    assert(typeof info === 'string', 'if info is included, it must be a string');
  }
  const channel = config.channel;
  if (channel) {
    const url = channel.url;
    assert(typeof url === 'string', 'channel.url must be a string');
    const route = channel.route;
    if (route) {
      assert(typeof route === 'string', 'if channel.route is included, it must be a string');
    }
  }
  const redirectUris = config.redirectUris;
  assert(typeof redirectUris === 'object', 'redirectUris must be included (either as a n array or an object -- only values are actually used)');
  assert(_.size(redirectUris), 'redirectUris must contain at least one item');
  _.each(redirectUris, item => {
    assert(typeof item === 'string', 'redirectUris need to be strings');
  });
};

const assertValidRefreshCallback = cb => {
  assert(cb, 'the refresh callback is missing');
  assert(typeof cb === 'function', 'the refresh callback is not a function');
};

module.exports = {
  assertValidUrl,
  assertValidAccountConfig,
  assertValidProviderConfig,
  assertValidRefreshCallback,
};
