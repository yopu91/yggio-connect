// src/index.js

const assert = require('assert');
const get = require('lodash.get');
const reduce = require('lodash.reduce');

const logger = require('./logger');
const yggioRequest = require('./yggio-request');
const yggioRoutes = require('./routes');
const serviceProvider = require('./service-provider');
const configValidator = require('./config-validator');

const init = async (config, providerSecret) => {
  // the yggio url is the minimal information
  assert(config, 'yggio config missing');
  configValidator.assertValidUrl(config);
  yggioRequest.setYggioUrl(config.url);
  // the other parts are only necessary if we want
  // to use the provider to interact with yggio, using
  // the builtin refresh capabilities.
  const accountDetails = get(config, 'account', null);
  const providerDetails = get(config, 'provider', null);
  const refreshCallback = get(config, 'refreshCallback', null);
  // If all three are missing, then only the raw routes will be usable
  if (!accountDetails && !providerDetails && !refreshCallback) {
    logger.warn('initializing yggio-connect WITHOUT provider functionality enabled');
    return;
  }
  // Otherwise all three should be validatable
  configValidator.assertValidAccountConfig(accountDetails);
  configValidator.assertValidProviderConfig(providerDetails);
  configValidator.assertValidRefreshCallback(refreshCallback);
  // and initialize the service provider
  return serviceProvider.init(accountDetails, providerDetails, refreshCallback, providerSecret);
};

const createWrappedRoutes = routes => {
  return reduce(routes, (acc, route, key) => {
    let wrappedRoute = null; // lazy load
    acc[key] = async (...args) => {
      if (!serviceProvider.isInitialized()) {
        throw new Error('Evaluate wrapped route: service provider is not initialized');
      }
      if (!wrappedRoute) {
        wrappedRoute = serviceProvider.wrapRoute(routes[key]);
      }
      return wrappedRoute(...args);
    };
    return acc;
  }, {});
};

module.exports = {
  init,
  // the raw routes, exposed for completeness
  rawRoutes: yggioRoutes,
  // the wrapped routes, which should generally be used
  routes: createWrappedRoutes(yggioRoutes),
  // and the provider utility functionality
  provider: {
    subscribe: serviceProvider.subscribe,
    redeemCode: serviceProvider.redeemCode,
    getDetails: serviceProvider.getProviderDetails,
  },
};
