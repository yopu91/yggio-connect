// yggio-request.js

const request = require('request-promise');
const get = require('lodash.get');

let yggioUrl = null;
const yggioRequest = async ({method, user, body, uri, qs, json, headers, formData}) => {
  if (!yggioUrl) {
    throw new Error('yggioUrl is not set');
  }
  if (!uri) {
    throw new Error('yggio api uri is missing');
  }
  const accessToken = get(user, 'accessToken');

  const baseReq = {
    method,
    baseUrl: yggioUrl,
    uri,
  };
  return request(Object.assign(
    baseReq,
    accessToken ? {auth: {bearer: accessToken}} : {},
    qs ? {qs} : {},
    body ? {body} : {},
    json ? {json} : {json: true},
    headers ? {headers} : {},
    formData ? {formData} : {}, // used only by setLogo (as of 2018)
  ));
};

module.exports = {
  setYggioUrl: url => {
    yggioUrl = url;
  },
  get: async requestObj => yggioRequest(Object.assign({}, requestObj, {
    method: 'GET',
  })),
  post: async requestObj => yggioRequest(Object.assign({}, requestObj, {
    method: 'POST',
  })),
  put: async requestObj => yggioRequest(Object.assign({}, requestObj, {
    method: 'PUT',
  })),
  delete: async requestObj => yggioRequest(Object.assign({}, requestObj, {
    method: 'DELETE',
  })),
};
