// logger.js

// just a simple wrapper for console.log

const log = (...args) => {
  console.log(...args);
};

const info = (...args) => {
  console.info(...args);
};

const warn = (...args) => {
  console.warn(...args);
};

const error = (...args) => {
  console.error(...args);
};

module.exports = {
  log,
  warn,
  error,
  info,
};
