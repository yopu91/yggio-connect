// routes.js
//
// This is just a flat file with a listing of yggio api commands,
// and should be treated as a reference/aid

const fs = require('fs');
const assert = require('assert');
const get = require('lodash.get');
const yggioRequest = require('./yggio-request');


// for token routes, we need to make sure that a token is supplied
const assertAccessToken = user => {
  assert(user && user.accessToken, 'The user access token is missing (user.accessToken)');
};

// ////
//
// Provider section
//
// ////

const setLogo = async (user, providerId, logoPath) => {
  assertAccessToken(user);
  assert(providerId, 'providerId is missing');
  assert(logoPath, 'logoPath is missing');
  const uri = '/api/providers/' + providerId + '/logo';
  const formData = {file: fs.createReadStream(logoPath)};
  return yggioRequest.post({user, uri, formData});
};

const registerProvider = async (user, provider) => {
  assertAccessToken(user);
  const uri = '/api/provider';
  const body = provider;
  return yggioRequest.post({user, uri, body});
};

// subscribes to a channel
const subscribe = async (user, subscription) => {
  assertAccessToken(user);
  assert(subscription.provider && subscription.iotnode && subscription.type, 'invalid subscription');
  const uri = '/api/channels';
  const body = subscription;
  return yggioRequest.post({user, uri, body});
};

// ////
//
// Channel section
//
// ////

const getChannels = async user => {
  assertAccessToken(user);
  const uri = '/api/channels';
  return yggioRequest.get({user, uri});
};

const getChannel = async (user, channelId) => {
  assertAccessToken(user);
  const uri = '/api/channels/' + channelId;
  return yggioRequest.get({user, uri});
};


// ////
//
// Iotnodes section
//
// ////

const command = async (user, commandData) => {
  // TODO: give a description of allowed commandData structure
  assertAccessToken(user);
  const uri = '/api/iotnodes/command';
  const body = commandData;
  return yggioRequest.put({user, uri, body});
};

// Just like totally gets me
const getMe = async (user) => {
  assertAccessToken(user);
  const uri = '/api/users/me';
  return yggioRequest.get({user, uri});
};

const logoutFromYggio = async (user, userId) => {
  assertAccessToken(user);
  const uri = '/api/users/' + userId + '/logout';
  return yggioRequest.post({user, uri});
};

const getNode = async (user, nodeId) => {
  assertAccessToken(user);
  const uri = '/api/iotnodes/' + nodeId;
  return yggioRequest.get({user, uri});
};

const getNodes = async (user) => {
  assertAccessToken(user);
  const uri = '/api/iotnodes';
  return yggioRequest.get({user, uri});
};

const getAccessibleNodes = async (user) => {
  assertAccessToken(user);
  const uri = '/api/iotnodes/shared';
  return yggioRequest.get({user, uri});
};

const createNode = async (user, iotnode) => {
  assertAccessToken(user);
  const uri = 'api/iotnodes';
  const body = iotnode;
  return yggioRequest.post({user, uri, body});
};

const updateNode = async (user, nodeId, updates) => {
  assertAccessToken(user);
  const uri = '/api/iotnodes/' + nodeId;
  const body = updates;
  return yggioRequest.put({user, uri, body});
};

const deleteNode = async (user, nodeId) => {
  assertAccessToken(user);
  const uri = '/api/iotnodes/' + nodeId;
  return yggioRequest.delete({user, uri});
};

// ////
//
// Oauth
//
// ////

const checkOAuthStatus = async (user, nodeType) => {
  assertAccessToken(user);
  const uri = '/api/oauth';
  const qs = {nodeType};
  return yggioRequest.get({user, uri, qs});
};

// ////
//
// Rules
//
// ////

const getRule = async (user, ruleId) => {
  assertAccessToken(user);
  const uri = '/api/rules/rules/' + ruleId;
  return yggioRequest.get({user, uri});
};

const getRules = async (user) => {
  assertAccessToken(user);
  const uri = '/api/rules/rules';
  return yggioRequest.get({user, uri});
};

const executeRule = async (user, ruleId) => {
  assertAccessToken(user);
  const uri = '/api/rules/rules/activate/' + ruleId;
  return yggioRequest.put({user, uri});
};


//
//
// ARE THESE USED ??????? - they should at least be renamed
const getScenarioTemplates = async (user) => {
  assertAccessToken(user);
  const uri = '/api/rules/templates';
  return yggioRequest.get({user, uri});
};
const createScenario = async (user, templateId) => {
  assertAccessToken(user);
  // Apply template does not remove existing rules
  const uri = '/api/rules/templates/' + templateId + '/apply';
  return yggioRequest.put({user, uri});
};
//
// ARE THESE USED ???????
//
//

const getRulesConditions = async (user) => {
  assertAccessToken(user);
  const uri = '/api/rules/conditions';
  return yggioRequest.get({user, uri});
};

// TODO: this one should switch args order to (user, conditionId, updates)
const updateRulesCondition = async (user, updates, conditionId) => {
  assertAccessToken(user);
  const uri = '/api/rules/conditions/' + conditionId;
  const body = updates;
  return yggioRequest.put({user, uri, body});
};

const createRulesCondition = async (user, condition) => {
  assertAccessToken(user);
  const uri = '/api/rules/conditions';
  const body = condition;
  return yggioRequest.post({user, uri, body});
};

const getRulesActions = async (user) => {
  assertAccessToken(user);
  const uri = '/api/rules/actions';
  return yggioRequest.get({user, uri});
};

const createRulesAction = async (user, action) => {
  assertAccessToken(user);
  const uri = '/api/rules/actions';
  const body = action;
  return yggioRequest.post({user, uri, body});
};

// TODO: this one should have args signature (user, actionId, updates)
const updateRulesAction = async (user, updates) => {
  assertAccessToken(user);
  assert(updates && updates._id, 'invalid updates object');
  const actionId = updates._id;
  const uri = '/api/rules/actions/' + actionId;
  const body = updates;
  return yggioRequest.put({user, uri, body});
};

const deleteRulesAction = async (user, actionId) => {
  assertAccessToken(user);
  const uri = '/api/rules/actions/' + actionId;
  return yggioRequest.delete({user, uri});
};

const createRule = async (user, rule) => {
  assertAccessToken(user);
  const uri = 'api/rules/rules';
  const body = rule;
  return yggioRequest.post({user, uri, body});
};

const deleteRule = async (user, ruleId) => {
  assertAccessToken(user);
  const uri = 'api/rules/rules/' + ruleId;
  return yggioRequest.delete({user, uri});
};

// TODO: this one should have args signature (user, ruleId, updates)
const updateRule = async (user, updates) => {
  assertAccessToken(user);
  assert(updates && updates._id, 'invalid updates object');
  const ruleId = updates._id;
  const uri = '/api/rules/rules/' + ruleId;
  const body = updates;
  return yggioRequest.put({user, uri, body});
};

// TODO: to follow naming conventions above, this one should be called createRulesTrigger
const addRuleTrigger = async (user, ruleId, trigger) => {
  assertAccessToken(user);
  const uri = '/api/rules/rules/' + ruleId + '/addtrigger';
  const body = trigger;
  return yggioRequest.put({user, uri, body});
};
// TODO: where are delete and update RulesTrigger ?????

// ////////
// SURELY THIS ONE IS DEAD ?????
// TODO: to follow naming conventions above, this one should be called addRulesEvent
const addRuleEvent = async (user, ruleId, evt) => {
  assertAccessToken(user);
  const uri = '/api/rules/rules/' + ruleId + '/addevent';
  const body = evt;
  return yggioRequest.put({user, uri, body});
};
// NEEDS A SWIFT KICK TO THE HEAD
// ////////

// ////
//
// Contacts
//
// ////

const getContacts = async (user) => {
  assertAccessToken(user);
  const uri = '/api/rules/contacts';
  return yggioRequest.get({user, uri});
};

const setContacts = async (user, contacts) => {
  assertAccessToken(user);
  const uri = '/api/rules/contacts';
  const body = contacts;
  return yggioRequest.put({user, uri, body});
};

// ////
// ////
//
// NON-TOKEN ROUTES
//
// ////
// ////

const login = async ({username, password}) => {
  assert(username && password, 'username or password is missing');
  const uri = '/auth/local';
  const body = {username, password};
  return yggioRequest.post({uri, body})
    .then(res => ({accessToken: res.token}));
};

const register = async ({username, password}) => {
  assert(username && password, 'username or password is missing');
  const uri = '/api/users';
  const body = {username, password};
  return yggioRequest.post({uri, body})
    .then(res => ({accessToken: res.token}));
};

const oauthAuthorization = async (clientId, redirectUri, responseType, accessToken) => {
  console.log(JSON.stringify({clientId, redirectUri, responseType, accessToken}, undefined, 2));
  assert(clientId && redirectUri && responseType && accessToken, 'clientId, redirectUri, responseType or accessToken is missing');
  const uri = '/auth/oauth';
  const qs = {
    client_id: clientId,
    redirect_uri: redirectUri,
    response_type: responseType
  };
  const user = {
    accessToken
  };
  return yggioRequest.get({uri, user, qs});
};

// this is just a utility function
const createProviderHeaders = provider => ({
  Authorization: 'Basic ' + Buffer.from(provider.client_id + ':' + provider.secret).toString('base64'),
});

const refreshAccessToken = async (provider, refreshToken) => {
  assert(provider && provider.client_id && provider.secret, 'invalid provider object');
  assert(refreshToken, 'refreshToken is missing');
  const uri = '/auth/oauth/token';
  const headers = createProviderHeaders(provider);
  const json = {
    grant_type: 'refresh_token',
    refresh_token: refreshToken,
  };
  const res = await yggioRequest.post({uri, headers, json});
  return {
    accessToken: get(res, 'access_token'),
    refreshToken: get(res, 'refresh_token'),
    expiresAt: get(res, 'expiresAt'),
  };
};

const oauthCode = (provider, code, redirectUri) => {
  assert(provider && provider.client_id && provider.secret, 'invalid provider object');
  assert(code, 'oauth code is missing');
  assert(redirectUri, 'redirectUri is missing');
  const uri = '/auth/oauth/token';
  const headers = createProviderHeaders(provider);
  const json = {
    code,
    grant_type: 'authorization_code',
    redirect_uri: redirectUri
  };
  return yggioRequest.post({uri, headers, json})
    .then(res => {
      const user = {
        accessToken: get(res, 'access_token'),
        refreshToken: get(res, 'refresh_token'),
        expiresAt: get(res, 'expiresAt'),
      };
      if (!user.accessToken) {
        return Promise.reject(new Error('Yggio user not found'));
      }
      return getMe(user).then(me => Object.assign({}, user, me));
    });
};

// ////
//
// //////// ALL ROUTES ////////
//
// ////

const nonTokenRoutes = {
  login,
  register,
  oauthAuthorization,
  refreshAccessToken,
  oauthCode,
};

const tokenRoutes = {
  // provider
  setLogo,
  registerProvider,
  subscribe,
  // channels
  getChannels,
  getChannel,
  // iotnodes
  command,
  getMe,
  logoutFromYggio,
  getNode,
  getNodes,
  getAccessibleNodes,
  createNode,
  updateNode,
  deleteNode,
  // oauth
  checkOAuthStatus,
  // rules
  getRule,
  getRules,
  executeRule,
  getScenarioTemplates,
  createScenario,
  getRulesConditions,
  updateRulesCondition,
  createRulesCondition,
  getRulesActions,
  createRulesAction,
  updateRulesAction,
  deleteRulesAction,
  createRule,
  deleteRule,
  updateRule,
  addRuleTrigger,
  addRuleEvent,
  // Contacts
  getContacts,
  setContacts,
};

module.exports = Object.assign({}, nonTokenRoutes, tokenRoutes);
