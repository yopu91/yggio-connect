// register-provider.js

const {registerProvider, setLogo} = require('../routes');
const logger = require('../logger');
const {assertValidProviderConfig} = require('../config-validator');

const register = async (serviceAccount, providerConfig) => {
  // validate the provider config (the serviceAccount is just user)
  assertValidProviderConfig(providerConfig);
  // begin registering
  const providerInfo = {
    name: providerConfig.name,
    info: providerConfig.info,
    redirect_uri: Object.values(providerConfig.redirectUris), // works for arrays as well
    channel: providerConfig.channel
  };
  return registerProvider(serviceAccount, providerInfo)
    .then(provider => {
      logger.info('registered service provider: ', providerConfig.name);
      // set the provider logo if appropriate
      if (providerConfig.logoPath) {
        return setLogo(serviceAccount, provider._id, providerConfig.logoPath)
          .catch(e => logger.error('setLogo error', e.message))
          .then(() => provider);
      }
      return provider;
    })
    .then(provider => {
      // and format the redirectUris
      const formatted = Object.assign({}, provider, {
        redirect_uri: undefined,
        redirectUris: providerConfig.redirectUris,
      });
      delete formatted.redirect_uri;
      return formatted;
    });
};

module.exports = register;
