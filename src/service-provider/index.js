// service-provider/index.js

const get = require('lodash.get');
const logger = require('../logger');
const {
  refreshAccessToken, subscribe, oauthCode
} = require('../routes');
const recursiveLogin = require('./recursive-login');
const registerProvider = require('./register-provider');

// all the mutable config stuff needed for a functioning provider
let accountConfig = null;
let providerConfig = null;
let refreshCallback = null;
// store the details returned through the setup process.
const entities = {};

// keep the setup function separate from the setConfigs, since it should
// be possible to use the raw routes (i.e. without any provider refresh-wrapping)
// if desired. But if we want to use the provider, then all the information/stuffings
// need to be included.
let setupBegun = false;
let setupFinished = false;
const init = async (account, provider, cb, providerSecret) => {
  // should this flag check be called in top level?
  if (setupFinished || setupBegun) {
    // we keep track of setupBegun, since setup stuff is async
    throw new Error('provider setup can only be run once');
  }
  setupBegun = true;

  // no validation is done here. Was done in top level.
  accountConfig = account;
  providerConfig = provider;
  refreshCallback = cb;

  // login the service-account user
  entities.account = await recursiveLogin(accountConfig);
  // and make sure the serviceProvider is registered
  entities.provider = await registerProvider(entities.account, providerConfig);
  const secret = get(entities, 'provider.secret', providerSecret);
  if (!secret) {
    throw new Error('No secret could be found when initializing provider, please provide a secret or new provider details');
  }
  entities.provider.secret = secret;
  // take note that setup is finished
  setupFinished = true;
  // and mostly for testing purposes, return the entities
  return entities;
};

// this is just a utility function to check if we have a refresh-error
const isRefreshError = err => {
  return err.statusCode === 401;
};

const initError = new Error('Provider has not been initialized');
const wrapRoute = route => {
  if (!setupFinished) {
    throw initError;
  }
  return async (inputUser, ...args) => {
    // since we are using lodash, make sure we make a copy or toObject.
    // This way, we do not mutate the input object at all.
    const isModel = get(inputUser, 'toObject');
    const user = isModel ? inputUser.toObject() : Object.assign({}, inputUser);

    // extract the access token
    const originalAccessToken = get(user, 'accessToken');
    // extract the refresh token
    const refreshToken = get(user, 'refreshToken');
    // no matter what we run the route straight off
    try {
      return await route(user, ...args);
    } catch (err) {
      // if there's no accessToken , just throw the error
      if (!originalAccessToken) {
        throw err;
      }
      // if it's not a refresh-error, then we just throw that error
      if (!isRefreshError(err)) {
        throw err;
      }
      // if we do not have a refreshToken, then we cannot refresh
      if (!refreshToken) {
        logger.error('refresh token is missing');
        throw new Error('refresh token is missing');
      }
      // now we know that we do need to refresh, and we should nominally have sufficient information
      const tokens = await refreshAccessToken(entities.provider, refreshToken);
      // allow the service to save the new accessToken (i.e. in user object)
      // and then rerun the original call
      const userCopy = Object.assign({}, user, tokens);
      await refreshCallback(user, tokens);
      return route(userCopy, ...args);
    }
  };
};

const wrappedSubscribe = async (user, yggioId, yggioType) => {
  if (!setupFinished) {
    throw initError;
  }
  const subscription = {
    iotnode: yggioId,
    provider: entities.provider._id,
    type: yggioType || 'IOTNODE',
  };
  return wrapRoute(subscribe)(user, subscription);
};

const redeemCode = async (code, redirectUri) => {
  if (!setupFinished) {
    throw initError;
  }
  return oauthCode(entities.provider, code, redirectUri);
};

const getProviderDetails = () => {
  if (!setupFinished) {
    throw initError;
  }
  return entities;
};

module.exports = {
  init,
  // expose functionality to create the wrapped routes
  isInitialized: () => setupFinished,
  wrapRoute,
  // the rest get exposed directly to the outside
  getProviderDetails,
  redeemCode,
  subscribe: wrappedSubscribe,
};
