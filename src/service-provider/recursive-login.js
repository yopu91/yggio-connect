// recursive-login.js
// provides login loop for service-provider.

const get = require('lodash.get');
const delay = require('delay');

const {login, register} = require('../routes');
const {assertValidAccountConfig} = require('../config-validator');
const logger = require('../logger');
const DEFAULT_RETRY_DELAY = 5000;

const recursiveLogin = async (accountInfo) => {
  assertValidAccountConfig(accountInfo);
  return register(accountInfo)
    .catch(err => {
      logger.warn('recursive-login: failed to register, trying to login instead');
      return login(accountInfo);
    })
    .then(serviceAccount => {
      const account = Object.assign({}, accountInfo, serviceAccount);
      logger.info('Successfully logged into account: ', get(account, 'username'));
      return account;
    })
    .catch(err => {
      const retryDelay = get(accountInfo, 'retryDelay', DEFAULT_RETRY_DELAY);
      logger.warn('recursive-login: Register/Login failed: ' + err.message + '. Retrying in ' + retryDelay + ' ms');
      return delay(retryDelay)
        .then(() => recursiveLogin(accountInfo));
    });
};

module.exports = recursiveLogin;
